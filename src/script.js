$(document).ready(function () {

    // левое меню на мобильном
    $(".left-slide-menu").click(function () {
        var toggle_el = $("#sidebar");
        var toggle_bar = $("#sidebar-toggle");
        $(toggle_el).toggleClass("open-sidebar");
        $(toggle_bar).toggleClass("open-sidebar");
    });


// автозаполнение формы выбора города
    $(function () {
        var availableTags = ["Волгоград", "Москва", "Самара", "Санкт-Петербург"]; //
        $("#city-search").autocomplete({ //на какой input:text назначить результаты списка
            source: availableTags
        });
    });

    // выбор вида заказа в зависимости ит нажатой кнопки(продукция, сувенирная продукция, маркетинговая поддержка)
    $('.tab_link').click(function (e) {
        e.preventDefault();
        SwitchTab($(this), $('.tab_link'), $('.ad_c'));
    });

    function SwitchTab(tab, tabs, content) {
        var cur = tab.attr('href');
        tabs.removeClass('active');
        tab.addClass('active');
        content.addClass('d-none');
        $('#' + cur).removeClass('d-none');
    }

    //сворачивает, разворачивает таблицу заказа в личном кабинете
    $(".date-order").click(function () {
        $(this).siblings(".table-order").slideToggle("slow");
    });

    var windowsize = $(window).width();

    // sliders
    if ($('.main-slider').length) {
        $('.main-slider').bxSlider({
            controls: false,
            pager: false,
            auto: true
        });
    }
    if ($('.news-slider').length) {
        $('.news-slider').bxSlider({
            nextText: '',
            prevText: '',
            pagerCustom: '#bx-pager',
            controls: true
        });
    }
    if ($('.packing-slider').length) {
        $('.packing-slider').bxSlider({
            controls: true,
            pager: false,
            maxSlides: 7,
            minSlides: 3,
            slideWidth: 90,
            responsive: true
        });
    }
    if (windowsize < 1000) {
        $('.products-slider').bxSlider({
            controls: true,
            pager: false,
            maxSlides: 1,
            minSlides: 1,
            responsive: true
        });
    } else {
        $('.products-slider').bxSlider({
            controls: true,
            pager: false,
            maxSlides: 2,
            minSlides: 1,
            slideWidth: 500,
            slideMargin: 50,
            responsive: true
        });
    }
    if (windowsize < 700) {
        $('.brand-mini-slider').bxSlider({
            controls: true,
            pager: false,
            maxSlides: 1,
            minSlides: 1,
            responsive: true
        });
    } else {
        $('.brand-mini-slider').bxSlider({
            controls: true,
            pager: false,
            maxSlides: 3,
            minSlides: 1,
            slideWidth: 300,
            responsive: true
        });
    }
    if ($('.labels_slider').length) {
        $('.labels_slider').bxSlider({
            nextText: '',
            prevText: '',
            pagerCustom: '#bx-pager',
            controls: true
        });
    }
    if ($('.news-slider').length) {
        $('.news-slider').bxSlider({
            nextText: '',
            prevText: '',
            pagerCustom: '#bx-pager',
            controls: true
        });
    }
    if (windowsize < 460) {
        $('.partners-slider').bxSlider({
            nextText: '',
            prevText: '',
            pagerCustom: '#bx-pager',
            controls: true,
            slideWidth: 200,
            slideMargin: 20,
            maxSlides: 1,
            minSlides: 1,
            responsive: true
        });
    } else if (windowsize < 750) {
        $('.partners-slider').bxSlider({
            nextText: '',
            prevText: '',
            pagerCustom: '#bx-pager',
            controls: true,
            slideWidth: 200,
            slideMargin: 20,
            maxSlides: 2,
            minSlides: 1,
            responsive: true
        });
    } else if (windowsize < 1000) {
        $('.partners-slider').bxSlider({
            nextText: '',
            prevText: '',
            pagerCustom: '#bx-pager',
            controls: true,
            slideWidth: 200,
            slideMargin: 20,
            maxSlides: 3,
            minSlides: 1,
            responsive: true
        });
    } else if (windowsize < 1200) {
        $('.partners-slider').bxSlider({
            nextText: '',
            prevText: '',
            pagerCustom: '#bx-pager',
            controls: true,
            slideWidth: 200,
            slideMargin: 20,
            maxSlides: 4,
            minSlides: 1,
            responsive: true
        });
    } else {
        $('.partners-slider').bxSlider({
            nextText: '',
            prevText: '',
            pagerCustom: '#bx-pager',
            controls: true,
            slideWidth: 200,
            slideMargin: 20,
            maxSlides: 5,
            minSlides: 1,
            responsive: true
        });
    }


//вызов таймера и дата отсчета
    var deadline = '2018-11-25 00:00:00';
    $('.countdown').downCount({
        date: deadline
    });


    // cloning selects in actions (клонирование списков товаров и производителей в фильтре акций)
    // use global var
    counter = 0;

    $("#add_sale-brand").click(function () {
        if (counter > 2) {
            return false;
        } else {
            counter++;
            $(".selectbrand").clone().attr('class', 'd-block mb-3 selectbrand' + counter).appendTo(".addselect_sale-brand");
            $(".addselect_sale-brand").append('<button type="button" style="position: relative; top: -40px" aria-label="Close" class="close row-remove" aria-hidden="true" onclick="delSelectBrand(); this.remove()">×</button>');
        }

    });

    counterProduct = 0;

    $("#add_product").click(function () {
        if (counterProduct > 2) {
            return false;
        } else {
            counterProduct++;
            $(".selectproduct").clone().attr('class', 'd-block mb-3 selectproduct' + counterProduct).appendTo(".addselect_product");
            $(".addselect_product").append('<button type="button" style="position: relative; top: -40px" aria-label="Close" class="close row-remove" aria-hidden="true" onclick="delSelectProduct(); this.remove()">×</button>');
        }

    });
// выделение месяца в фильтре акций
    $(".month-action, .packing").click(function () {
        $(this).toggleClass('alert-danger');
    });

// inputmask in modal forms
    $('#phone, #review-phone, #enter-phone, #cooperation-phone, #distrib-phone').inputmask('+7 (999) 999-99-99');

    // появление дополнительного поля при выборе пункта "хочу покупать"
    $("#cooperation-buy").click(function () {
        $("div.form-want-buy").show();
    });
    $("#supply, #service, #work").click(function () {
        $("div.form-want-buy").hide();
    });

// opening characters of product at hover on picture
    $('.img_product').hover(
        function () {
            $('.characters_product').toggle('show');
        }
    );

    // выбор промежутка времени в форме на странице Маркетинговая поддержка
    $('#date_timepicker_start').datetimepicker({
        format: 'd/m/Y',
        onShow: function () {
            this.setOptions({
                maxDate: $('#date_timepicker_end').val() ? $('#date_timepicker_end').val() : false
            })
        },
        timepicker: false
    });
    $('#date_timepicker_end').datetimepicker({
        format: 'd/m/Y',
        onShow: function () {
            this.setOptions({
                minDate: $('#date_timepicker_start').val() ? $('#date_timepicker_start').val() : false
            })
        },
        timepicker: false
    });

    $('#date_action_start').datetimepicker({
        format: 'd/m/Y',
        onShow: function () {
            this.setOptions({
                maxDate: $('#date_action_end').val() ? $('#date_action_end').val() : false
            })
        },
        timepicker: false
    });
    $('#date_action_end').datetimepicker({
        format: 'd/m/Y',
        onShow: function () {
            this.setOptions({
                minDate: $('#date_action_start').val() ? $('#date_action_start').val() : false
            })
        },
        timepicker: false
    });

});


// function of action timer
(function ($) {

    $.fn.downCount = function (options, callback) {
        var settings = $.extend({
            date: null,
            offset: null
        }, options);

        // Throw error if date is not set
        if (!settings.date) {
            $.error('Date is not defined.');
        }

        // Throw error if date is set incorectly
        if (!Date.parse(settings.date)) {
            $.error('Incorrect date format, it should look like this, 12/24/2018 12:00:00.');
        }

        // Save container
        var container = this;


        var currentDate = function () {
            // get client's current date
            var date = new Date();

            // turn date to utc
            var utc = date.getTime() + (date.getTimezoneOffset() * 60000);

            // set new Date object
            var new_date = new Date(utc + (3600000 * settings.offset))

            return new_date;
        };


        function countdown() {
            var target_date = new Date(settings.date), // set target date
                current_date = currentDate(); // get fixed current date

            // difference of dates
            var difference = target_date - current_date;

            // if difference is negative than it's pass the target date
            if (difference < 0) {
                // stop timer
                clearInterval(interval);

                if (callback && typeof callback === 'function') callback();

                return;
            }

            // basic math variables
            var _second = 1000,
                _minute = _second * 60,
                _hour = _minute * 60,
                _day = _hour * 24;

            // calculate dates
            var days = Math.floor(difference / _day),
                hours = Math.floor((difference % _day) / _hour),
                minutes = Math.floor((difference % _hour) / _minute),
                seconds = Math.floor((difference % _minute) / _second);

            // fix dates so that it will show two digets
            days = (String(days).length >= 2) ? days : '0' + days;
            hours = (String(hours).length >= 2) ? hours : '0' + hours;
            minutes = (String(minutes).length >= 2) ? minutes : '0' + minutes;
            seconds = (String(seconds).length >= 2) ? seconds : '0' + seconds;


            // set to DOM
            container.find('.days').text(days);
            container.find('.hours').text(hours);
            container.find('.minutes').text(minutes);
            container.find('.seconds').text(seconds);

        };

        // start
        var interval = setInterval(countdown, 1000);
    };


})(jQuery);

// delete select
function delSelectBrand() {
    // console.log(counter);
    $(".selectbrand" + counter).remove();
    counter--;
}

function delSelectProduct() {
    $(".selectproduct" + counterProduct).remove();
    counterProduct--;
}








