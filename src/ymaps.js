//yandex maps

ymaps.ready(init);
ymaps.ready(init1);

function init1() {
    var myMap = new ymaps.Map('miniMap', {
            center: [55.733780, 37.648959],
            zoom: 5,
            controls: ['geolocationControl', 'zoomControl']
        })

    var geolocation = ymaps.geolocation;

    geolocation.get({
        provider: 'yandex',
        mapStateAutoApply: true
    }).then(function (result) {
        // Красным цветом пометим положение, вычисленное через ip.
        result.geoObjects.options.set('preset', 'islands#redCircleIcon');
        result.geoObjects.get(0).properties.set({
            balloonContentBody: 'Мое местоположение'
        });
        myMap.geoObjects.add(result.geoObjects);
        userAddress = result.geoObjects.get(0).properties.get('name');
        userCoordinates = result.geoObjects.position;
    });
}

function init() {

    var myMap = new ymaps.Map('map', {
            center: [55.733780, 37.648959],
            zoom: 5,
            controls: ['geolocationControl', 'searchControl', 'zoomControl']
        }),
        deliveryPoint = new ymaps.GeoObject({
            geometry: {type: 'Point'},
            properties: {iconCaption: 'Адрес'}
        }, {
            preset: 'islands#blackDotIconWithCaption',
            draggable: true,
            iconCaptionMaxWidth: '215'
        }),
        searchControl = myMap.controls.get('searchControl');
    searchControl.options.set({noPlacemark: true, placeholderContent: 'Введите адрес доставки'});
    myMap.geoObjects.add(deliveryPoint);

    var GodObj = {};

    var geolocation = ymaps.geolocation;

    geolocation.get({
        provider: 'yandex',
        mapStateAutoApply: true
    }).then(function (result) {
        // Красным цветом пометим положение, вычисленное через ip.
        result.geoObjects.options.set('preset', 'islands#redCircleIcon');
        result.geoObjects.get(0).properties.set({
            balloonContentBody: 'Мое местоположение'
        });
        myMap.geoObjects.add(result.geoObjects);
        userAddress = result.geoObjects.get(0).properties.get('name');
        userCoordinates = result.geoObjects.position;
        GodObj.lala = userCoordinates;
        // $('#tow').html('Ваш город: <a data-toggle="modal" data-target="#Modal" id="data" href="#">' + userAddress + '</a>');
    });

// Описания дистрибьюторов можно хранить в формате JSON, а потом генерировать
// из описания геообъекты с помощью ymaps.geoQuery.
    distr = ymaps.geoQuery({
        type: 'FeatureCollection',
        features: [{
            type: 'Feature',
            properties: {
                balloonContent: 'Лебедева Кристина <br> ООО "СОГЛАСИЕ2016", 440013, Пензенская обл, Пенза г, Светлая ул, дом No 50, офис 430 <br> 89631098577 <br> voronov27@yandex.ru ' + '  <button type="button" class="btn btn-primary d-block" data-toggle="modal" data-target="#ModalDistributor">\n' +
                    ' Хочу у Вас закупать\n' + '</button> '
            },
            geometry: {
                type: 'Point',
                coordinates: [53.222266, 45.059297]
            }
        }, {
            type: 'Feature',
            properties: {
                balloonContent: 'Лебедева Кристина <br> ИП Шабанов <br> Саратовская обл, Саратов г, Комсомольская ул, дом № 52, офис 313 "А" <br> 89172877064 <br> kazanmilk2017@mail.ru ' + '  <button type="button" class="btn btn-primary d-block" data-toggle="modal" data-target="#ModalDistributor">\n' +
                    ' Хочу у Вас закупать\n' + '</button> '
            },
            geometry: {
                type: 'Point',
                coordinates: [51.532705, 46.045189]
            }
        }, {
            type: 'Feature',
            properties: {
                balloonContent: 'Лебедева Кристина <br> ИП Шабанов <br> г. Астрахань, ул. Рыбинская д12 <br> 8-927-565-55-00 <br> bor2@yandex.ru ' + '  <button type="button" class="btn btn-primary d-block" data-toggle="modal" data-target="#ModalDistributor">\n' +
                    ' Хочу у Вас закупать\n' + '</button> '
            },
            geometry: {
                type: 'Point',
                coordinates: [46.122580, 48.240648]
            }
        }, {
            type: 'Feature',
            properties: {
                balloonContent: 'Лебедева Кристина <br> ИП Шабанов <br> г. Астрахань, ул. Рыбинская д12 <br> 8-927-565-55-00 <br> bor2@yandex.ru ' + '  <button type="button" class="btn btn-primary d-block" data-toggle="modal" data-target="#ModalDistributor">\n' +
                    ' Хочу у Вас закупать\n' + '</button> ',
                outOfRegion: true
            },
            geometry: {
                type: 'Point',
                coordinates: [55.728210, 37.671800]
            }
        }]
        // Сразу добавим точки на карту.
    }).addToMap(myMap);


    myLocation = ymaps.geoQuery(ymaps.geocode(geolocation))
    // Нужно дождаться ответа от сервера и только потом обрабатывать полученные результаты.
        .then(findClosestObjects);

    setTimeout(function () {

        function onZonesLoad(json) {
            // Добавляем зоны на карту.
            deliveryZones = ymaps.geoQuery(json).addToMap(myMap);
            // Задаём цвет и контент балунов полигонов.
            deliveryZones.each(function (obj) {
                var color = obj.options.get('fillColor');
                color = color.substring(0, color.length - 2);
                obj.options.set({fillColor: color, fillOpacity: 0.2});
                obj.properties.set('balloonContent');
                obj.properties.set('balloonContentHeader', obj.properties.get('name'));

            });


            // Проверим попадание результата поиска в одну из зон доставки.
            searchControl.events.add('resultshow', function (e) {
                highlightResult(searchControl.getResultsArray()[e.get('index')]);
            });

            // Проверим попадание метки геолокации в одну из зон доставки.
            myMap.controls.get('geolocationControl').events.add('locationchange', function (e) {
                // highlightResult(e.get('geoObjects').get(0));
                obj.options.set({fillColor: color, fillOpacity: 0.2});
            });

            // При перемещении метки сбрасываем подпись, содержимое балуна и перекрашиваем метку.
            deliveryPoint.events.add('dragstart', function () {
                deliveryPoint.properties.set({iconCaption: '', balloonContent: ''});
                deliveryPoint.options.set('iconColor', 'black');
            });

            // По окончании перемещения метки вызываем функцию выделения зоны доставки.
            deliveryPoint.events.add('dragend', function () {
                highlightResult(deliveryPoint);
            });

            //
            var coordinate = GodObj.lala;
            var outOfRegion;
            var macroRegion = false;
            deliveryZones.each(function (res) {
                    if (res.geometry.contains(coordinate)) {
                        macroRegion = res.properties._data.macroRegion;
                    }
                }
            );

            // if we out of all macroregions
            if (!macroRegion) {
                outOfRegion = true;
            } else {
                deliveryZones.each(function (res) {
                    if (res.properties._data.macroRegion === macroRegion) {
                        res.options.set({fillOpacity: 0.6});
                    }
                });
            }

            // Определяем ближайшего дистрибьютора
            var closestObject = false;
            var result = ymaps.geoQuery(distr).addToMap(myMap);
            result.setOptions('visible', false);

            //Если город клиента не входит ни в один макрорегион, открывается пиндык Москвы
            if (outOfRegion) {
                // пиндык на Москву
                distr = ymaps.geoQuery({
                    type: 'Feature',
                    properties: {
                        balloonContent: 'Центральный офис 111024, г. Москва, ш. Энтузиастов, д.21, стр. 2, ком. 505 (495) 940-17-52, (495) 940-13-18 Отдел продаж - s.sidorov@mir-vkusa.net        \n' +
                            'Маркетинг - m.zanegina@mir-vkusa.net ' + '  <button type="button" class="btn btn-primary d-block" data-toggle="modal" data-target="#ModalDistributor">\n' +
                            ' Хочу у Вас закупать\n' + '</button> ',
                        outOfRegion: true
                    },
                    geometry: {
                        type: 'Point',
                        coordinates: [55.728210, 37.671800]
                    }
                }).addToMap(myMap);
            } else {
                result.each(function (res) {
                    deliveryZones.each(function (del) {
                        if ((del.properties._data.macroRegion === macroRegion) && (del.geometry.contains(res.geometry._coordinates))) {
                            closestObject = res;
                        }
                    })
                });
                if (!closestObject)
                    closestObject = result.getClosestTo(coordinate);

                closestObject.options.set('visible', true);
            }


            function highlightResult(obj) {
                // Сохраняем координаты переданного объекта.
                var coords = obj.geometry.getCoordinates(),
                    // Находим полигон, в который входят переданные координаты.
                    polygon = deliveryZones.searchContaining(coords).get(0);

                if (polygon) {
                    // Уменьшаем прозрачность всех полигонов, кроме того, в который входят переданные координаты.
                    deliveryZones.setOptions('fillOpacity', 0.2);
                    polygon.options.set('fillOpacity', 0.6);
                    // Перемещаем метку с подписью в переданные координаты и перекрашиваем её в цвет полигона.
                    deliveryPoint.geometry.setCoordinates(coords);
                    deliveryPoint.options.set('iconColor', polygon.options.get('fillColor'));
                    // Задаем подпись для метки.
                    if (typeof(obj.getThoroughfare) === 'function') {
                        setData(obj);
                    } else {
                        // Если вы не хотите, чтобы при каждом перемещении метки отправлялся запрос к геокодеру,
                        // закомментируйте код ниже.
                        ymaps.geocode(coords, {results: 1}).then(function (res) {
                            var obj = res.geoObjects.get(0);
                            setData(obj);
                        });
                    }
                } else {
                    // Если переданные координаты не попадают в полигон, то задаём стандартную прозрачность полигонов.
                    deliveryZones.setOptions('fillOpacity', 0.2);
                    // Перемещаем метку по переданным координатам.
                    deliveryPoint.geometry.setCoordinates(coords);
                    // Задаём контент балуна и метки.
                    deliveryPoint.properties.set({
                        iconCaption: 'Доставка транспортной компанией',
                        balloonContent: 'Cвяжитесь с оператором',
                        balloonContentHeader: ''
                    });
                    // Перекрашиваем метку в чёрный цвет.
                    deliveryPoint.options.set('iconColor', 'black');
                }

                function setData(obj) {
                    var address = [obj.getThoroughfare(), obj.getPremiseNumber(), obj.getPremise()].join(' ');
                    if (address.trim() === '') {
                        address = obj.getAddressLine();
                    }
                    deliveryPoint.properties.set({
                        iconCaption: address,
                        balloonContent: address,
                    });
                }
            }


        }

        $.ajax({
            url: 'data.json',
            dataType: 'json',
            success: onZonesLoad
        });

    }, 5555);

    function findClosestObjects() {
        // Найдем в выборке кафе, ближайшее к найденной станции метро,
        // и откроем его балун.
        distr.getClosestTo(geolocation.get(0)).balloon.open();

        // Будем открывать балун кафе, который ближе всего к месту клика
        myMap.events.add('click', function (event) {
            distr.getClosestTo(event.get('coords')).balloon.open();
        });
    }

}